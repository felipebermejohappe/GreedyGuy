﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public enum TypeOfGate { SelectionGate, EntranceGate, Outgate };
    public TypeOfGate thisGateType;

    public string levelName = "";
    [SerializeField]
    GateSO gateSO = null;
    [SerializeField]
    SpriteRenderer doorTop = null;
    [SerializeField]
    GameObject fence = null;

    bool isOpen = false;
    bool canBeOpen = false;

    private void OnEnable()
    {
        //PlayerPrefs.DeleteAll();
        
        if (LevelManager.Instance != null && thisGateType == TypeOfGate.Outgate)
        {
            LevelManager.Instance.AllCoinsRecolected += UpdateGateState;
            LevelManager.Instance.AllCoinsRecolected += OpenGate;
        }

        if (thisGateType != TypeOfGate.EntranceGate)
        {
            switch (PlayerPrefs.GetInt(name + "State", 0))
            {
                case 0:
                    doorTop.sprite = gateSO.TopDoorLocked;
                    fence.SetActive(true);
                    break;
                case 1:
                    doorTop.sprite = gateSO.TopDoorUnlocked;
                    fence.SetActive(false);
                    break;
                case 2:
                    doorTop.sprite = gateSO.TopDoorCompleted;
                    fence.SetActive(false);
                    break;
            }
        }
        else
        {
            doorTop.sprite = gateSO.TopDoorUnlocked;
            fence.SetActive(false);

            if (PlayerPrefs.GetInt("GateToLevel" + levelName + "State", 0) == 0)
            {
                PlayerPrefs.SetInt("GateToLevel" + levelName + "State", 1);
            }
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Open"))
        {
            if (canBeOpen && isOpen)
            {
                CheckFirstLevelCompleted();

                CheckToAddLevelAndPutCompleted();

                NextScene();
            }
        }
    }

    private void OnDisable()
    {
        if (LevelManager.Instance != null && thisGateType == TypeOfGate.Outgate)
        {
            LevelManager.Instance.AllCoinsRecolected -= UpdateGateState;
        }
    }

    private void UpdateGateState()
    {
        doorTop.sprite = gateSO.TopDoorUnlocked;
        fence.SetActive(false);
    }

    private void OpenGate()
    {
        isOpen = true;
    }

    private void CheckFirstLevelCompleted()
    {
        if (PlayerPrefs.GetInt("LevelsCompleted", 0) == 0)
        {
            PlayerPrefs.SetInt("GateToMainMenuState", 1);
            PlayerPrefs.SetInt("GateToLevelSelectionState", 1);
        }
    }

    private void CheckToAddLevelAndPutCompleted()
    {
        if (PlayerPrefs.GetInt("GateToLevel" + levelName + "State", 0) != 2)
        {
            PlayerPrefs.SetInt("LevelsCompleted", PlayerPrefs.GetInt("LevelsCompleted", 0) + 1);
            PlayerPrefs.SetInt("GateToLevel" + levelName + "State", 2);
        }
    }

    private void NextScene()
    {
        if (MySceneManager.Instance.ActiveScene() +1 < MySceneManager.Instance.TotalScenes())
        {
            MySceneManager.Instance.GoToScene(MySceneManager.Instance.ActiveScene() + 1);
        }
        else
        {
            MySceneManager.Instance.GoToScene(0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14)
        {
            canBeOpen = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14)
        {
            canBeOpen = false;
        }
    }
}
