﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSquareMovement : Platform
{
    enum Direction { Up, Down, Left, Right };
    Direction myDirection;

    public enum InSquareSituation { Norwest, North, Noreast, West, East, Southwest, South, Southeast }
    public InSquareSituation inSquareSituation;

    Vector3 startPosition;
    public float height;
    public float width;
    public float velocity;
    public bool clockWise;
    float topLimit;
    float bottomLimit;
    float leftLimit;
    float rightLimite;

    // Use this for initialization
    void Start()
    {
        //transform.position = posicionInicial;
        startPosition = transform.position;

        switch (inSquareSituation)
        {
            case InSquareSituation.Norwest:
                topLimit = startPosition.y;
                bottomLimit = startPosition.y - height;
                leftLimit = startPosition.x;
                rightLimite = startPosition.x + width;
                if (clockWise)
                {
                    myDirection = Direction.Right;
                }
                else
                {
                    myDirection = Direction.Down;
                }
                break;
            case InSquareSituation.North:
                topLimit = startPosition.y;
                bottomLimit = startPosition.y - height;
                leftLimit = startPosition.x - width / 2;
                rightLimite = startPosition.x + width / 2;
                if (clockWise)
                {
                    myDirection = Direction.Right;
                }
                else
                {
                    myDirection = Direction.Left;
                }
                break;
            case InSquareSituation.Noreast:
                topLimit = startPosition.y;
                bottomLimit = startPosition.y - height;
                leftLimit = startPosition.x - width;
                rightLimite = startPosition.x;
                if (clockWise)
                {
                    myDirection = Direction.Down;
                }
                else
                {
                    myDirection = Direction.Left;
                }
                break;
            case InSquareSituation.West:
                topLimit = startPosition.y + height / 2;
                bottomLimit = startPosition.y - height / 2;
                leftLimit = startPosition.x;
                rightLimite = startPosition.x + width;
                if (clockWise)
                {
                    myDirection = Direction.Up;
                }
                else
                {
                    myDirection = Direction.Down;
                }
                break;
            case InSquareSituation.East:
                topLimit = startPosition.y + height / 2;
                bottomLimit = startPosition.y - height / 2;
                leftLimit = startPosition.x - width;
                rightLimite = startPosition.x;
                if (clockWise)
                {
                    myDirection = Direction.Down;
                }
                else
                {
                    myDirection = Direction.Up;
                }
                break;
            case InSquareSituation.Southwest:
                topLimit = startPosition.y + height;
                bottomLimit = startPosition.y;
                leftLimit = startPosition.x;
                rightLimite = startPosition.x + width;
                if (clockWise)
                {
                    myDirection = Direction.Up;
                }
                else
                {
                    myDirection = Direction.Right;
                }
                break;
            case InSquareSituation.South:
                topLimit = startPosition.y + height;
                bottomLimit = startPosition.y;
                leftLimit = startPosition.x - width / 2;
                rightLimite = startPosition.x + width / 2;
                if (clockWise)
                {
                    myDirection = Direction.Left;
                }
                else
                {
                    myDirection = Direction.Right;
                }
                break;
            case InSquareSituation.Southeast:
                topLimit = startPosition.y + height;
                bottomLimit = startPosition.y;
                leftLimit = startPosition.x - width;
                rightLimite = startPosition.x;
                if (clockWise)
                {
                    myDirection = Direction.Left;
                }
                else
                {
                    myDirection = Direction.Up;
                }
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            switch (myDirection)
            {
            case Direction.Up:
                transform.position = new Vector3(transform.position.x, transform.position.y + velocity * Time.deltaTime, transform.position.z);
                if (transform.position.y >= topLimit)
                {
                    transform.position = new Vector3(transform.position.x, topLimit, transform.position.z);
                    if (clockWise)
                    {
                        myDirection = Direction.Right;
                    }
                    else
                    {
                        myDirection = Direction.Left;
                    }
                }
                break;
            case Direction.Down:
                transform.position = new Vector3(transform.position.x, transform.position.y - velocity * Time.deltaTime, transform.position.z);
                if (transform.position.y <= bottomLimit)
                {
                    transform.position = new Vector3(transform.position.x, bottomLimit, transform.position.z);
                    if (clockWise)
                    {
                        myDirection = Direction.Left;
                    }
                    else
                    {
                        myDirection = Direction.Right;
                    }
                }
                break;
            case Direction.Left:
                transform.position = new Vector3(transform.position.x - velocity * Time.deltaTime, transform.position.y, transform.position.z);
                if (transform.position.x <= leftLimit)
                {
                    transform.position = new Vector3(leftLimit, transform.position.y, transform.position.z);
                    if (clockWise)
                    {
                        myDirection = Direction.Up;
                    }
                    else
                    {
                        myDirection = Direction.Down;
                    }
                }
                break;
            case Direction.Right:
                transform.position = new Vector3(transform.position.x + velocity * Time.deltaTime, transform.position.y, transform.position.z);
                if (transform.position.x >= rightLimite)
                {
                    transform.position = new Vector3(rightLimite, transform.position.y, transform.position.z);
                    if (clockWise)
                    {
                        myDirection = Direction.Down;
                    }
                    else
                    {
                        myDirection = Direction.Up;
                    }
                }
                break;
            }
        }
    }
}
