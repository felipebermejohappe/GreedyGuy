﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorTrick : MonoBehaviour
{
    [SerializeField]
    Animator animatorTrickAnimator = null;
    [SerializeField]
    Animator heroAnimator = null;

    [SerializeField]
    GameObject tittle = null;
    [SerializeField]
    GameObject startButton = null;
    [SerializeField]
    GameObject levelsButton = null;

    [SerializeField]
    GameObject CamMovementButtons = null;
    [SerializeField]
    GameObject HomeButton = null;

    //Animation actualAnimation = null;

    int animationState = 0;

    public void IsIdle()
    {
        heroAnimator.SetBool("IsIdle", true);
        heroAnimator.SetBool("IsWalking", false);
        heroAnimator.SetBool("IsRunning", false);
        heroAnimator.SetBool("IsJumpping", false);
        heroAnimator.SetBool("IsFalling", false);
    }

    public void IsWalking()
    {
        heroAnimator.SetBool("IsIdle", false);
        heroAnimator.SetBool("IsWalking", true);
        heroAnimator.SetBool("IsRunning", false);
        heroAnimator.SetBool("IsJumpping", false);
        heroAnimator.SetBool("IsFalling", false);
    }

    public void IsRunning()
    {
        heroAnimator.SetBool("IsIdle", false);
        heroAnimator.SetBool("IsWalking", false);
        heroAnimator.SetBool("IsRunning", true);
        heroAnimator.SetBool("IsJumpping", false);
        heroAnimator.SetBool("IsFalling", false);
    }

    public void IsJumpping()
    {
        heroAnimator.SetBool("IsIdle", false);
        heroAnimator.SetBool("IsWalking", false);
        heroAnimator.SetBool("IsRunning", false);
        heroAnimator.SetBool("IsJumpping", true);
        heroAnimator.SetBool("IsFalling", false);
    }

    public void IsFalling()
    {
        heroAnimator.SetBool("IsIdle", false);
        heroAnimator.SetBool("IsWalking", false);
        heroAnimator.SetBool("IsRunning", false);
        heroAnimator.SetBool("IsJumpping", false);
        heroAnimator.SetBool("IsFalling", true);
    }

    public void GoToLevelSelection()
    {
        if (animationState == 0)
        {
            animatorTrickAnimator.SetBool("SecondPath", false);
            animatorTrickAnimator.SetBool("ReturnPath", false);
            animatorTrickAnimator.SetBool("ToDark", false);
            animatorTrickAnimator.enabled = true;
        }
        else
        {
            animatorTrickAnimator.SetBool("SecondPath", true);
            animatorTrickAnimator.SetBool("ReturnPath", false);
            animatorTrickAnimator.SetBool("ToDark", false);
            animatorTrickAnimator.enabled = true;
        }
    }

    public void GoToLevelSelectionValidation()
    {
        if (PlayerPrefs.GetInt("LevelsCompleted", 0) == 0)
        {
            animatorTrickAnimator.SetBool("SecondPath", false);
            animatorTrickAnimator.SetBool("ReturnPath", true);
            animatorTrickAnimator.SetBool("ToDark", false);
            animationState = 1;
        }
    }

    public void ActivateMainButtons()
    {
        tittle.SetActive(true);
        startButton.SetActive(true);
        levelsButton.SetActive(true);
    }

    public void ActivateHomeAndMoveButtons()
    {
        animatorTrickAnimator.StopPlayback();
        CamMovementButtons.SetActive(true);
        HomeButton.SetActive(true);
    }

    public void UnenableAnimator()
    {
        animatorTrickAnimator.enabled = false;
    }

    public void TellSceneManagerToChangeScene (int sceneIndex)
    {
        MySceneManager.Instance.GoToScene(sceneIndex);
    }

    public void ToDarkAndRunLevel1()
    {
        animatorTrickAnimator.SetBool("SecondPath", false);
        animatorTrickAnimator.SetBool("ReturnPath", false);
        animatorTrickAnimator.SetBool("ToDark", true);
        animatorTrickAnimator.enabled = true;
    }

    public void TellSceneManagerToMainMenu()
    {
        MySceneManager.Instance.MainMenu();
    }

    public void ReturnToMainMenu ()
    {
        animatorTrickAnimator.SetBool("SecondPath", false);
        animatorTrickAnimator.SetBool("ReturnPath", true);
        CamMovementButtons.SetActive(false);
        HomeButton.SetActive(false);
        animationState = 1;
        animatorTrickAnimator.enabled = true;
    }
}
