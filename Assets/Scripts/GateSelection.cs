﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSelection : MonoBehaviour
{
    [SerializeField]
    Animator animatorTrickAnimator = null;

    [SerializeField]
    AnimationClip animationToPlay = null;

    string levelName = "";

    private void Awake()
    {
        levelName = GetComponent<Gate>().levelName;    
    }

    private void OnMouseDown()
    {
        if (PlayerPrefs.GetInt("GateToLevel" + levelName + "State", 0) != 0)
        {
            animatorTrickAnimator.enabled = true;
            animatorTrickAnimator.Play(animationToPlay.name);
        }
    }
}
