﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject[] ObjectsContained = null;

    bool canBeOpen = false;
    bool isOpen = false;
    AudioSource openingAudio;

    private void OnEnable()
    {
        openingAudio = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Open"))
        {
            if (canBeOpen)
            {
                if (!isOpen)
                {
                    isOpen = !isOpen;
                    gameObject.GetComponent<Animator>().enabled = true;
                    openingAudio.Play();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer ==  14)
        {
            canBeOpen = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14)
        {
            canBeOpen = false;
        }
    }

    private void ActivateObjects()
    {
        foreach (GameObject objectToActivate in ObjectsContained)
        {
            objectToActivate.SetActive(!objectToActivate.activeSelf);
        }
    }
}
