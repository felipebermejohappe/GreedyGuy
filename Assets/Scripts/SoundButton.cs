﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundButton : MonoBehaviour
{
    [SerializeField]
    AudioSource mainMusic = null;
    [SerializeField]
    GameObject muteOFFButton = null;
    [SerializeField]
    GameObject muteONButton = null;

    private void OnEnable()
    {
        if (PlayerPrefs.GetInt("MainMusicState", 1) == 1)
        {
            muteOFFButton.SetActive(true);
            muteONButton.SetActive(false);
            MuteOFFMainMusic();
        }
        else
        {
            muteOFFButton.SetActive(false);
            muteONButton.SetActive(true);
            MuteOnMainMusic();
        }
    }

    public void MuteOFFMainMusic()
    {
        mainMusic.mute = false;
        PlayerPrefs.SetInt("MainMusicState", 1);
    }

    public void MuteOnMainMusic()
    {
        mainMusic.mute = true;
        PlayerPrefs.SetInt("MainMusicState", 0);
    }
}
