﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameCanvasController : MonoBehaviour
{
    public static InGameCanvasController Instance { get; private set; }

    [SerializeField]
    GameObject optionsButtons = null;

    [SerializeField]
    GameObject gameOver = null;

    [SerializeField]
    Text actualAmount = null;
    [SerializeField]
    Text totalAmount = null;

    [SerializeField]
    GameObject infoPanel = null;

    private void Awake()
    {
        Instance = this;    
    }

    private void OnEnable()
    {
        HeroBehaviour.HeroIsDeadRelease += ActivateGameOver;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && !infoPanel.activeSelf)
        {
            optionsButtons.SetActive(!optionsButtons.activeSelf);
        }
    }

    void ActivateGameOver()
    {
        gameOver.SetActive(true);
        HeroBehaviour.HeroIsDeadRelease -= ActivateGameOver;
    }

    public void RetryLevel()
    {
        MySceneManager.Instance.RetryLevel();
    }

    public void MainMenu()
    {
        MySceneManager.Instance.MainMenu();
    }

    public void RefreshAllCoinsInUI(int actualAmount, int totalAmount)
    {
        RefreshActualCoinsInUI(actualAmount);
        RefreshTotalCoinsInUI(totalAmount);
    }

    public void RefreshActualCoinsInUI(int amount)
    {
        actualAmount.text = ConvertedMaskForText(amount);
    }

    public void RefreshTotalCoinsInUI(int amount)
    {
        totalAmount.text = ConvertedMaskForText(amount);
    }

    public string ConvertedMaskForText(int enteredInteger)
    {
        string valueToReturn = "00";

        switch (enteredInteger)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                valueToReturn = "0" + enteredInteger.ToString();
                break;
            default:
                valueToReturn = enteredInteger.ToString();
                break;
        }

        return valueToReturn;
    }

    private void OnDisable()
    {
        {
            HeroBehaviour.HeroIsDeadRelease -= ActivateGameOver;
        }
    }
}
