﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    public delegate void AllCoinsRecollectedDelegate();
    public event AllCoinsRecollectedDelegate AllCoinsRecolected;

    
    GameObject[] coinsArray = null;
    int totalCoins = 0;
    public int recollectedCoins = 0;

    [SerializeField]
    GameObject[] gameObjectsToEnable = null;
    
    private void Awake()
    {
        Instance = this;

        FindAllCoins();
        CountTotalCoins();
        DeactiveCoins();
        EnableObjects();
        InGameCanvasController.Instance.RefreshAllCoinsInUI(recollectedCoins, totalCoins);
    }

    private void Update()
    {
        if (recollectedCoins == totalCoins)
        {
            if (AllCoinsRecolected != null)
            {
                AllCoinsRecolected();
            }
        }
    }

    void FindAllCoins()
    {
        coinsArray = GameObject.FindGameObjectsWithTag("Coin");
    }

    void CountTotalCoins()
    {
        totalCoins = coinsArray.Length;
    }

    void DeactiveCoins()
    {
        foreach (GameObject coin in coinsArray)
        {
            coin.SetActive(false);
        }
    }

    public void AddCoinToCollection()
    {
        recollectedCoins++;
        InGameCanvasController.Instance.RefreshActualCoinsInUI(recollectedCoins);
    }

    void EnableObjects()
    {
        foreach (GameObject individualObject in gameObjectsToEnable)
        {
            individualObject.SetActive(true);
        }
    }
}
