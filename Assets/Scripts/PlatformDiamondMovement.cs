﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDiamondMovement : Platform
{
    Vector3 startPosition;
   
    public enum InDiamondSituation { Northwest, North, Northeast, West, East, Southwest, South, Southeast }
    public InDiamondSituation mySituation;
    public float verticalDiagonal;
    public float horizontalDiagonal;
    public float velocity;

    public bool clockWise;

    enum Direccion { Northwest, Northeast, Southwest, Southeast };
    Direccion miDireccion;

    float topLimit;
    float bottomLimit;
    float leftLimit;
    float rightLimit;

    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;

        switch (mySituation)
        {
            case InDiamondSituation.Northwest:
                topLimit = startPosition.y + verticalDiagonal * 1 / 4;
                bottomLimit = startPosition.y - verticalDiagonal * 3 / 4;
                leftLimit = startPosition.x - horizontalDiagonal * 1 / 4;
                rightLimit = startPosition.x + horizontalDiagonal * 3 / 4;
                if (clockWise)
                {
                    miDireccion = Direccion.Northeast;
                }
                else
                {
                    miDireccion = Direccion.Southwest;
                }
                break;
            case InDiamondSituation.North:
                topLimit = startPosition.y;
                bottomLimit = startPosition.y - verticalDiagonal;
                leftLimit = startPosition.x - horizontalDiagonal * 1 / 2;
                rightLimit = startPosition.x + horizontalDiagonal * 1 / 2;
                if (clockWise)
                {
                    miDireccion = Direccion.Southeast;
                }
                else
                {
                    miDireccion = Direccion.Southwest;
                }
                break;
            case InDiamondSituation.Northeast:
                topLimit = startPosition.y + verticalDiagonal * 1 / 4;
                bottomLimit = startPosition.y - verticalDiagonal * 3 / 4;
                leftLimit = startPosition.x - horizontalDiagonal * 3 / 4;
                rightLimit = startPosition.x + horizontalDiagonal * 1 / 4;
                if (clockWise)
                {
                    miDireccion = Direccion.Southeast;
                }
                else
                {
                    miDireccion = Direccion.Northwest;
                }
                break;
            case InDiamondSituation.West:
                topLimit = startPosition.y + verticalDiagonal * 1 / 2;
                bottomLimit = startPosition.y - verticalDiagonal * 1 / 2;
                leftLimit = startPosition.x;
                rightLimit = startPosition.x + horizontalDiagonal;
                if (clockWise)
                {
                    miDireccion = Direccion.Northeast;
                }
                else
                {
                    miDireccion = Direccion.Southeast;
                }
                break;
            case InDiamondSituation.East:
                topLimit = startPosition.y + verticalDiagonal * 1 / 2;
                bottomLimit = startPosition.y - verticalDiagonal * 1 / 2;
                leftLimit = startPosition.x - horizontalDiagonal;
                rightLimit = startPosition.x;
                if (clockWise)
                {
                    miDireccion = Direccion.Southwest;
                }
                else
                {
                    miDireccion = Direccion.Northwest;
                }
                break;
            case InDiamondSituation.Southwest:
                topLimit = startPosition.y + verticalDiagonal * 3 / 4;
                bottomLimit = startPosition.y - verticalDiagonal * 1 / 4;
                leftLimit = startPosition.x - horizontalDiagonal * 1 / 4;
                rightLimit = startPosition.x + horizontalDiagonal * 3 / 4;
                if (clockWise)
                {
                    miDireccion = Direccion.Northwest;
                }
                else
                {
                    miDireccion = Direccion.Southeast;
                }
                break;
            case InDiamondSituation.South:
                topLimit = startPosition.y + verticalDiagonal;
                bottomLimit = startPosition.y;
                leftLimit = startPosition.x - horizontalDiagonal * 1 / 2;
                rightLimit = startPosition.x + horizontalDiagonal * 1 / 2;
                if (clockWise)
                {
                    miDireccion = Direccion.Northwest;
                }
                else
                {
                    miDireccion = Direccion.Northeast;
                }
                break;
            case InDiamondSituation.Southeast:
                topLimit = startPosition.y + verticalDiagonal * 3 / 4;
                bottomLimit = startPosition.y - verticalDiagonal * 1 / 4;
                leftLimit = startPosition.x - horizontalDiagonal * 3 / 4;
                rightLimit = startPosition.x + horizontalDiagonal * 1 / 4;
                if (clockWise)
                {
                    miDireccion = Direccion.Southwest;
                }
                else
                {
                    miDireccion = Direccion.Northeast;
                }
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (miDireccion)
        {
            case Direccion.Northwest:
                transform.position = new Vector3(transform.position.x - (horizontalDiagonal * velocity * Time.deltaTime), transform.position.y + (verticalDiagonal * velocity * Time.deltaTime), transform.position.z);
                if (transform.position.y > topLimit)
                {
                    transform.position = new Vector3(rightLimit - horizontalDiagonal / 2, topLimit, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Southeast;
                    }
                    else
                    {
                        miDireccion = Direccion.Southwest;
                    }
                }
                else if (transform.position.x < leftLimit)
                {
                    transform.position = new Vector3(leftLimit, topLimit - verticalDiagonal / 2, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Northeast;
                    }
                    else
                    {
                        miDireccion = Direccion.Southeast;
                    }
                }
                break;
            case Direccion.Northeast:
                transform.position = new Vector3(transform.position.x + (horizontalDiagonal * velocity * Time.deltaTime), transform.position.y + (verticalDiagonal * velocity * Time.deltaTime), transform.position.z);
                if (transform.position.y > topLimit)
                {
                    transform.position = new Vector3(rightLimit - horizontalDiagonal / 2, topLimit, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Southeast;
                    }
                    else
                    {
                        miDireccion = Direccion.Southwest;
                    }

                }
                else if (transform.position.x > rightLimit)
                {
                    transform.position = new Vector3(rightLimit, topLimit - verticalDiagonal / 2, transform.position.z);
                    
                    if (clockWise)
                    {
                        miDireccion = Direccion.Southwest;
                    }
                    else
                    {
                        miDireccion = Direccion.Northwest;
                    }
                }
                break;
            case Direccion.Southeast:
                transform.position = new Vector3(transform.position.x + (horizontalDiagonal * velocity * Time.deltaTime), transform.position.y - (verticalDiagonal * velocity * Time.deltaTime), transform.position.z);
                if (transform.position.y < bottomLimit)
                {
                    transform.position = new Vector3(rightLimit - horizontalDiagonal / 2, bottomLimit, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Northwest;
                    }
                    else
                    {
                        miDireccion = Direccion.Northeast;
                    }

                }
                else if (transform.position.x > rightLimit)
                {
                    transform.position = new Vector3(rightLimit, topLimit - verticalDiagonal / 2, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Southwest;
                    }
                    else
                    {
                        miDireccion = Direccion.Northwest;
                    }
                }
                break;
            case Direccion.Southwest:
                transform.position = new Vector3(transform.position.x - (horizontalDiagonal * velocity * Time.deltaTime), transform.position.y - (verticalDiagonal * velocity * Time.deltaTime), transform.position.z);
                if (transform.position.y < bottomLimit)
                {
                    transform.position = new Vector3(rightLimit - horizontalDiagonal / 2, bottomLimit, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Northwest;
                    }
                    else
                    {
                        miDireccion = Direccion.Northeast;
                    }
                }
                else if (transform.position.x < leftLimit)
                {
                    transform.position = new Vector3(leftLimit, topLimit - verticalDiagonal / 2, transform.position.z);

                    if (clockWise)
                    {
                        miDireccion = Direccion.Northeast;
                    }
                    else
                    {
                        miDireccion = Direccion.Southeast;
                    }
                }
                break;
        }
    }
}
