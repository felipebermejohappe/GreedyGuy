﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CamMovement : MonoBehaviour
{
    [SerializeField]
    Button leftButton = null;

    [SerializeField]
    Button rightButton = null;

    [SerializeField]
    Camera myCamera = null;

    [SerializeField]
    Transform[] posiblePositionsForCamera= null;

    int indexPPFC = 0;

    private void OnEnable()
    {
        if (indexPPFC == 0)
        {
            leftButton.gameObject.SetActive(false);
            myCamera.transform.position = posiblePositionsForCamera[0].position;
        }
    }

    public void PreviousPosition()
    {
        if (indexPPFC == posiblePositionsForCamera.Length -1)
        {
            rightButton.gameObject.SetActive(true);
        }
        indexPPFC--;
     

        myCamera.transform.position = posiblePositionsForCamera[indexPPFC].position;

        if (indexPPFC == 0)
        {
            leftButton.gameObject.SetActive(false);
        }
    }

    public void NextPosition()
    {
        if (indexPPFC == 0)
        {
             leftButton.gameObject.SetActive(true);
        }
        indexPPFC++;
      
        myCamera.transform.position = posiblePositionsForCamera[indexPPFC].position;

        if (indexPPFC == posiblePositionsForCamera.Length - 1)
        {
            rightButton.gameObject.SetActive(false);
        }
    }
}
