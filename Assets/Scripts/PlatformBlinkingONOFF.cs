﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBlinkingONOFF : Platform
{
    public float timeOn  = 1;
    public float timeOff = 1;

    bool startBlinking = true;

    BoxCollider2D boxCollD2 = null;
    SpriteRenderer spRenderer = null;
    [SerializeField]
    BoxCollider2D Border = null;

    private void Awake()
    {
        boxCollD2 = GetComponent<BoxCollider2D>();
        spRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            if (startBlinking)
            {
                startBlinking = false;
                StartCoroutine(BlinkONOFF());
            }
        }
        
    }

    IEnumerator BlinkONOFF()
    {
        yield return new WaitForSeconds(timeOn);
        boxCollD2.enabled = false;
        spRenderer.enabled = false;
        Border.enabled = false;
        yield return new WaitForSeconds(timeOff);
        boxCollD2.enabled = true;
        spRenderer.enabled = true;
        Border.enabled = true;
        startBlinking = true;
    }
}
