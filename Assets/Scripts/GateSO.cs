﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GateSO : ScriptableObject
{
    public Sprite TopDoorCompleted;
    public Sprite TopDoorUnlocked;
    public Sprite TopDoorLocked;
}
