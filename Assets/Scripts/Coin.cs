﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    AudioSource coinSound;

    bool isRecollected = false;

    private void OnEnable()
    {
        coinSound = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14)
        {
            if (isRecollected == false)
            {
                isRecollected = true;
                LevelManager.Instance.AddCoinToCollection();
                coinSound.Play();
                StartCoroutine(DeactiveCoin());
            }
        }

    }

    IEnumerator DeactiveCoin()
    {
        yield return new WaitForSeconds(0.15f);
        gameObject.SetActive(false);
    }
}
