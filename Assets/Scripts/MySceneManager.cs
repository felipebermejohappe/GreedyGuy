﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
    int actualScene = 0;
    public static MySceneManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            actualScene = SceneManager.GetActiveScene().buildIndex;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void GoToScene(int indexScene)
    {
        actualScene = indexScene;
        SceneManager.LoadScene(indexScene);
    }

    public int ActiveScene()
    {
        return actualScene;
    }

    public int TotalScenes()
    {
        return SceneManager.sceneCountInBuildSettings;
    }

    public void RetryLevel()
    {
        GoToScene(actualScene);
    }

    public void MainMenu()
    {
        GoToScene(0);
    }
}
