﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroBehaviour : MonoBehaviour
{
    public delegate void HeroIsDeadDelegate();
    public static event HeroIsDeadDelegate HeroIsDeadRelease;

    //Public for behaviour measuring
    public int heroLifes = 1;
    public float heroVelocity = 2.5f;
    public float jumpAmount  = 5.5f;
    public float runIncrease = 1.50f;

    //Inputs
    float heroMovement = 0;
    bool isJumping = false;
    float isRunning = 0;

    //Components
    Animator myAnimator;
    Rigidbody2D rB2D;
    AudioSource JumpAudio;

    //Auxliary variables
    string myAnimatorState = "IsIdle";
    bool canJump = true;
    bool isOnGround = true;
    bool heroIsPLayable = true;
    int jumpNumbers = 0;

    //Constants
    const string Idle = "IsIdle";
    const string Walk = "IsWalking";
    const string Run  = "IsRunning";
    const string Jump = "IsJumpping";
    const string Fall = "IsFalling";

    private void Awake()
    {
        myAnimator = GetComponent<Animator>();
        myAnimatorState = Idle;
        rB2D = GetComponent<Rigidbody2D>();
        JumpAudio = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (heroLifes != 0)
        {
            if (heroIsPLayable)
            {
                heroMovement = Input.GetAxis("Horizontal");

                if (canJump && jumpNumbers < 2)
                {
                    //isJumping = Input.GetAxisRaw("Jump");
                    isJumping = Input.GetKeyDown(KeyCode.Space);

                }

                isRunning = Input.GetAxisRaw("Run");

                if (heroMovement > 0)
                {
                    GetComponent<SpriteRenderer>().flipX = false;
                }
                else if (heroMovement < 0)
                {
                    GetComponent<SpriteRenderer>().flipX = true;
                }

                if (canJump && isJumping && jumpNumbers < 2)
                {
                    MakeHeroJump();
                }

                rB2D.velocity = new Vector2(heroMovement * heroVelocity * Mathf.Pow(runIncrease, isRunning), rB2D.velocity.y);

                CheckAnimator();
            }
        }
        else
        {
            if (HeroIsDeadRelease != null)
            {
                HeroIsDeadRelease();
                rB2D.Sleep();
            }
        }
    }

    void CheckAnimator()
    {
        if (myAnimator != null)
        {
            if (isOnGround)
            {
                if (rB2D.velocity.x != 0)
                {
                    if (isRunning == 1)
                    {
                        myAnimator.SetBool(Run, true);
                        myAnimator.SetBool(myAnimatorState, false);
                        myAnimatorState = Run;
                    }
                    else
                    {
                        myAnimator.SetBool(Walk, true);
                        myAnimator.SetBool(myAnimatorState, false);
                        myAnimatorState = Walk;
                    }
                }
                else
                {
                    myAnimator.SetBool(Idle, true);
                    myAnimator.SetBool(myAnimatorState, false);
                    myAnimatorState = Idle;
                }
            }
            else if (rB2D.velocity.y > 0)
            {
                myAnimator.SetBool(Jump, true);
                myAnimator.SetBool(myAnimatorState, false);
                myAnimatorState = Jump;
            }
            else
            {
                myAnimator.SetBool(Fall, true);
                myAnimator.SetBool(myAnimatorState, false);
                myAnimatorState = Fall;
            } 
        }
    }

    IEnumerator MakeHeroJumpCorrutine()
    {
        yield return new WaitForSeconds(0.75f);
        canJump = true;
    }

    void MakeHeroJump()
    {
        canJump = false;
        isOnGround = false;
        jumpNumbers++;
        rB2D.velocity = new Vector2(rB2D.velocity.x/jumpNumbers, jumpAmount/ jumpNumbers);
        JumpAudio.Play();
        StartCoroutine(MakeHeroJumpCorrutine());
    }

    public void HeroIsPLayable(bool state)
    {
        heroIsPLayable = state;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 11)
        {
            isOnGround = true;
            jumpNumbers = 0;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 11)
        {
            isOnGround = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 15)
        {
            heroLifes--;
        }
    }
}
